
fun same_string(s1 : string, s2 : string) =
    s1 = s2

(* put your solutions for problem 1 here *)
datatype nombres = Empty| colas of string*nombres;


fun ejercicio1(cadena: string, lista: string list) =
	let
		fun buscar(cadena, lista, aux_lista) =
				case lista of
					[] => []
					| head::cola => if same_string(head,cadena) 
										then aux_lista @ cola
									else buscar(cadena, cola, aux_lista @ [head])
		in
			case buscar(cadena, lista, []) of
				[] => NONE
				| algo => SOME algo
	end

fun ejercicio2(cadena: string, name_listas: string list list) =
	case name_listas of
		[] => []
		| head::cola => case ejercicio1(cadena, head) of
							NONE => ejercicio2(cadena,cola)
							| SOME algo => algo @ ejercicio2(cadena, cola)

fun ejercicio3(name_listas: string list list, cadena: string) =
	let
		fun ejercicio3_tr(name_listas: string list list, cadena: string, aux_lista: string list) =
				case name_listas of
					[] => aux_lista
					| head::cola => case ejercicio1(cadena, head) of
										NONE => ejercicio3_tr(cola, cadena, aux_lista)
										(*tail recursion*)
										| SOME algo => ejercicio3_tr(cola, cadena, aux_lista @ algo)
		in
			ejercicio3_tr(name_listas, cadena, [])
	end

fun ejercicio4(listas, {first= p1, middle= p2, last= p3}) =
	let
		val full_name = {first= p1, middle= p2, last= p3}
		fun hacer_tuplas(substitutions: string list) =
				case substitutions of
				[] => []
				| head::cola => {first= head, middle= p2, last= p3} :: hacer_tuplas(cola)
		in
		    [full_name] @ hacer_tuplas(ejercicio2(p1,listas))
	end

(* you may assume that Num is always used with values 2, 3, ..., 10
   though it will not really come up *)
datatype suit = Clubs | Diamonds | Hearts | Spades
datatype rank = Jack | Queen | King | Ace | Num of int 
type card = suit * rank

datatype color = Red | Black
datatype move = Discard of card | Draw 

exception IllegalMove

(* put your solutions for problem 2 here *)
